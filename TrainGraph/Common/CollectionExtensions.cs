﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainGraph.Common
{
    public static class CollectionExtensions
    {

        public static TValue GetOrAdd<TKey, TValue>(this IDictionary<TKey, TValue>  dict, TKey key, Func<TKey, TValue> valueFactory)
        {
            if (!dict.ContainsKey(key))
            {
                dict[key] = valueFactory(key);
            }
            return dict[key];
        }

        public static T GetOrAdd<T>(this ICollection<T> collection, Func<T> newElement, Func<T, bool> equalityComparer)
        {
            if (!collection.Any(equalityComparer)) { 
                collection.Add(newElement());
            }
            return collection.First(equalityComparer);         
        }

        /// <summary>
        /// Add to the tail of the colletion (similar to " :: " in Scala)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection"></param>
        /// <param name="element"></param>
        /// <returns></returns>
        public static ICollection<T> Append<T>(this ICollection<T> collection, T element)
        {
            return new List<T>(collection).Concat(new List<T>() {element}).ToList();
            //collection.Add(element);
            //return collection;
        }

        /// <summary>
        /// Add to the head of the colletion
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection"></param>
        /// <param name="element"></param>
        /// <returns></returns>
        public static ICollection<T> Prepend<T>(this ICollection<T> collection, T element)
        {
            return new T[]{element}.Concat(collection).ToList();
      
        }
    }
}
