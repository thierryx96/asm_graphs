﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainGraph.Common
{
    static class ConsoleUtils
    {
        public static TOutput TryRead<TOutput>(string message, Func<string, TOutput> parse)
        {
            while (true)
            {
                Console.WriteLine(message);
                TOutput output;
                var line = Console.ReadLine();
                try
                {
                    output = parse(line);
                }
                catch (Exception)
                {
                    Console.WriteLine($"Invalid entry: '{line}'. Please try again.");
                    continue;
                }
                return output;
            }
        }

        public static int TryReadNumber(int min = 0, int max = int.MaxValue)
        {
            while (true)
            {

                Console.Write($"Please enter a number between '{min}' and '{max}':");
                var key = Console.ReadKey().Key;
                Console.WriteLine("");
                int number;
                if (!int.TryParse(key.ToString().Replace("D", string.Empty), NumberStyles.Integer, CultureInfo.InvariantCulture, out number))
                {
                    Console.WriteLine($"Incorrect entry '{key}', it must be a valid number.");
                    continue;
                }

                if (!(number >= min && number <= max))
                {
                    Console.WriteLine($"Number: '{number}' must be between '{min}' and '{max}'");
                    continue;
                }
                return number;
            }
        }
    }
}
