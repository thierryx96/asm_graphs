﻿using System;
using System.Collections.Generic;
using System.Linq;
using TrainGraph.Common;
using TrainGraph.Graphs;

namespace TrainGraph.Graphs
{


    /// <summary>
    ///     Define a graph, as a data structure
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    public class Graph<TKey> : IGraph<TKey> where TKey : struct, IEquatable<TKey>
    {
        // node's key  - connectedNodes' key and distance (a connection can appear more than one)
        //  private readonly HashSet<Node> _nodes = new HashSet<Node>();
        private readonly IDictionary<TKey, ICollection<Connection<TKey>>> _nodes;

        public Graph()
        {
            _nodes = new Dictionary<TKey, ICollection<Connection<TKey>>>();
        }

        public Graph(IEnumerable<Edge<TKey>> edges)
        {
            _nodes = new Dictionary<TKey, ICollection<Connection<TKey>>>();
            foreach (var edge in edges)
            {
                AddEdge(edge.From, edge.To, edge.Distance);
            }
        }

        

        /// <summary>
        /// Add a new connection between two nodes
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="distance"></param>
        public void AddEdge(TKey from, TKey to, int distance)
        {
            var fromNode = _nodes.GetOrAdd(from, _ => new List<Connection<TKey>>()); // source
            _nodes.GetOrAdd(to, _ => new List<Connection<TKey>>()); // destination

            fromNode.Add(new Connection<TKey>(to, distance));
        }

        /// <summary>
        ///     Return a list of edge between two connected nodes (if not directly connected : empty)
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        public IList<Edge<TKey>> GetEdgesBetween(TKey from, TKey to) =>
            GetConnections(from)
                .Where(v => v.To.Equals(to))
                .OrderBy(v => v.Distance)
                .Select(v => new Edge<TKey>(from, to, v.Distance))
                .ToList();


        /// <summary>
        /// Get the shortest path following a set of way points (nodes on the path)
        /// </summary>
        /// <param name="wayPoints">nodes that the path must follow</param>
        /// <returns></returns>
        public IList<Edge<TKey>> GetShortestPathBy(ICollection<TKey> wayPoints)
        {
            var path = GetShortestPath(wayPoints.First(), wayPoints.Skip(1).ToList()).ToList();
            return path.Count + 1 == wayPoints.Count ? path : null;
        }

        /// <summary>
        /// Recusively get the shortest path from a source 
        /// following a list of way points (nodes on the path)
        /// </summary>
        /// <param name="source">source</param>
        /// <param name="wayPoints">nodes that the path must follow</param>
        /// <returns></returns>
        private ICollection<Edge<TKey>> GetShortestPath(TKey source, ICollection<TKey> wayPoints)
        {
            // exit condition 
            if (wayPoints.Count == 0)
            {
                return new List<Edge<TKey>>();
            }
            var edge = GetEdgesBetween(source, wayPoints.First()).FirstOrDefault(); // shortest
            return edge == null
                ? new List<Edge<TKey>>()
                : GetShortestPath(wayPoints.First(), wayPoints.Skip(1).ToList()).Prepend(edge);
        }


        /// <summary>
        /// Find all path between a source and a destination 
        /// satisfying multiple criteria (max nodes, max distance, exclude cycles)
        /// </summary>
        /// <param name="source"></param>
        /// <param name="destination"></param>
        /// <param name="nodesCountLimit">limit the search by nodes in the path [static] </param>
        /// <param name="distanceLimit">limit the search by distance [static] </param>
        /// <param name="excludeCycles">don't visit a node already in the path collection [static] </param>
        /// <returns></returns>
        public IEnumerable<ICollection<Connection<TKey>>> GetAllPathsBetween(
            TKey source,
            TKey destination,
            int? nodesCountLimit,
            int? distanceLimit,
            bool excludeCycles
            )
        {
            ICollection<ICollection<Connection<TKey>>> foundPaths = new List<ICollection<Connection<TKey>>>();
            SearchPathsBetween(foundPaths, source, 0, destination, null, nodesCountLimit, distanceLimit, excludeCycles);
            return foundPaths;
        }

        /// <summary>
        /// Recusively find all the paths between two nodes (source and destination), 
        /// satisfying multiple criteria (max nodes, max distance, exclude cycles)
        /// the path found will be added to the found path list
        /// </summary>
        /// <param name="foundPaths">where we add collected paths</param>
        /// <param name="currentSource"></param>
        /// <param name="currentDistance"></param>
        /// <param name="destination"></param>
        /// <param name="path">the path being collected</param>
        /// <param name="nodesCountLimit">limit the search by nodes in the path [static] </param>
        /// <param name="distanceLimit">limit the search by distance [static] </param>
        /// <param name="excludeCycles">don't visit a node already in the path collection [static] </param>
        private void SearchPathsBetween(
            ICollection<ICollection<Connection<TKey>>> foundPaths, // 
            TKey currentSource, // recusive (dynamic)
            int currentDistance, // recusive (dynamic)
            TKey destination, // static 
            ICollection<Connection<TKey>> path, // recusive (dynamic)
            int? nodesCountLimit, // static 
            int? distanceLimit, // static 
            bool excludeCycles // static
            )
        {
            var currentPath = path != null
                ? path.Append(new Connection<TKey>(currentSource, currentDistance))
                : new List<Connection<TKey>>();

            if (currentSource.Equals(destination) && currentPath.Count > 1)
            {
                foundPaths.Add(currentPath);
            }

            foreach (var connection in GetConnections(currentSource))
            {
                var isIncluded = currentPath.Any(n => n.To.Equals(connection.To));

                if ((!nodesCountLimit.HasValue || currentPath.Count < nodesCountLimit) &&
                    (!distanceLimit.HasValue || currentDistance + connection.Distance < distanceLimit) &&
                    (!excludeCycles || !isIncluded))
                {
                    SearchPathsBetween(foundPaths, connection.To, currentDistance + connection.Distance, destination,
                        currentPath, nodesCountLimit, distanceLimit, excludeCycles);
                }
            }
        }

        private IEnumerable<Connection<TKey>> GetConnections(TKey key)
        {
            return _nodes[key];
        }

       
    }
}