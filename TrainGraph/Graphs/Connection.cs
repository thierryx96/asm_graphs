﻿using System;

namespace TrainGraph.Graphs
{
    /// <summary>
    /// represent a connection, i. e. info of connection TO a node
    /// [distance] -> (to)
    /// </summary>
    public class Connection<TKey>
    {
        public Connection(TKey to, int distance)
        {
            To = to;
            Distance = distance;
        }

        public TKey To { get; }
        public int Distance { get; }

        public Tuple<TKey, int> ToTuple() => new Tuple<TKey, int>(To, Distance);

        public override string ToString() => $"[{Distance}] -> {To}";
    }
}
