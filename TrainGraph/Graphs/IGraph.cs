﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainGraph.Graphs
{
    public interface IGraph<TKey> where TKey : struct, IEquatable<TKey>
    {
        /// <summary>
        /// Add a new connection between two nodes
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="distance"></param>
        void AddEdge(TKey from, TKey to, int distance);

        /// <summary>
        ///     Return a list of edge between two connected nodes (if not directly connected : empty)
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        IList<Edge<TKey>> GetEdgesBetween(TKey from, TKey to);

        /// <summary>
        /// Get the shortest path following a set of way points (nodes on the path)
        /// </summary>
        /// <param name="wayPoints">nodes that the path must follow</param>
        /// <returns></returns>
        IList<Edge<TKey>> GetShortestPathBy(ICollection<TKey> wayPoints);

        /// <summary>
        /// Find all path between a source and a destination 
        /// satisfying multiple criteria (max nodes, max distance, exclude cycles)
        /// </summary>
        /// <param name="source"></param>
        /// <param name="destination"></param>
        /// <param name="nodesCountLimit">limit the search by nodes in the path [static] </param>
        /// <param name="distanceLimit">limit the search by distance [static] </param>
        /// <param name="excludeCycles">don't visit a node already in the path collection [static] </param>
        /// <returns></returns>
        IEnumerable<ICollection<Connection<TKey>>> GetAllPathsBetween(
            TKey source,
            TKey destination,
            int? nodesCountLimit,
            int? distanceLimit,
            bool excludeCycles
            );
    }
}
