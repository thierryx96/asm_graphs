﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainGraph.Graphs
{
    /// <summary>
    /// represent an edge (connection between 2 nodes)
    /// (from) -> [distance] -> (to)
    /// </summary>
    public class Edge<TKey> : Connection<TKey>
    {
        public Edge(TKey from, TKey to, int distance) : base(to, distance)
        {
            From = from;
        }

        public TKey From { get; }
        public override string ToString() => $"{From} " + base.ToString();
    }


}
