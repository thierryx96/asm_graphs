﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainGraph.Common;
using TrainGraph.Graphs;

namespace TrainGraph.Train
{
    public class RailNetwork
    {
        private readonly IGraph<char> _graph;

        private static Edge<char> ParseEdge(string s)
        {
            var nodesStr = s.Substring(0, 2).ToCharArray();
            return new Edge<char>(nodesStr[0], nodesStr[1], int.Parse(s.Substring(2)));
        }

        private static Graph<char> ParseGraph(string s)
        {
            var edges = s.Split(new[] {",", Environment.NewLine, ";", " "}, StringSplitOptions.RemoveEmptyEntries)
                .Select(ParseEdge).ToArray(); // parsing 

            return new Graph<char>(edges);
        }

        public RailNetwork(string s)
        {
            _graph = ParseGraph(s);
        }


        public static RailNetwork LoadFromFile(string path)
        {
            return new RailNetwork(File.ReadAllText(path));
        }

        public int? GetDistance(IList<char> route)
        {
            var path = _graph.GetShortestPathBy(route);
            return path?.Sum(v => v.Distance);
        }

        public IEnumerable<Trip> GetAllTripsBetweenWithMaxStops(char from, char to, int maxStops)
        {
            return _graph.GetAllPathsBetween(from, to, maxStops, null, false).Select(p => new Trip(from, p.Select(e => e.ToTuple())));
        }

        public IEnumerable<Trip> GetAllTripsBetweenWithExactStops(char from, char to, int exactStops)
        {
            return _graph.GetAllPathsBetween(from, to, exactStops, null, false)
                .Where(p => p.Count == exactStops) // exclude trips with less than the exact number of stops
                .Select(p => new Trip(from, p.Select(e => e.ToTuple())));
        }

        public IEnumerable<Trip> GetAllTripsBetweenWithMaxDistance(char from, char to, int maxDistance)
        {
            return _graph.GetAllPathsBetween(from, to, null, maxDistance, false).Select(p => new Trip(from, p.Select(e => e.ToTuple())));
        }

        public IEnumerable<Trip> GetAllTripsBetweenWithExactDistance(char from, char to, int exactDistance)
        {
            return _graph.GetAllPathsBetween(from, to, null, exactDistance, false).Select(p => new Trip(from, p.Select(e => e.ToTuple())));
        }

        public Trip GetShortestPathBetween(char from, char to)
        {
            var trips = _graph.GetAllPathsBetween(from, to, null, null, true).Select(p => new Trip(from, p.Select(e => e.ToTuple())));           
            return trips.OrderBy(v => v.TotalDistance).FirstOrDefault();
        }
    }
}
