﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainGraph.Common;
using TrainGraph.Graphs;

namespace TrainGraph.Train
{
    /// <summary>
    /// Define a trip, journey on the rail network
    /// </summary>
    public class Trip
    {
        public Trip(ICollection<Edge<char>> segments)
        {
            Segments = segments;
        }

        public Trip(char origin, IEnumerable<Tuple<char, int>> segments)            
        {
            Segments = new List<Edge<char>>();
            var previous = origin;
            foreach (var segment in segments)
            {
                Segments.Add(new Edge<char>(previous, segment.Item1, segment.Item2));
            }

        }

        public ICollection<Edge<char>> Segments { get; }

        /// <summary>
        /// Including
        /// </summary>
        public ICollection<char> Stops =>
                Segments.Aggregate(new List<char>(),
                    (a, c) => a.Count == 0 ? new List<char>() { c.From, c.To } : a.Append(c.To).ToList());

        public int TotalDistance => Segments.Last().Distance;

        public override string ToString()
            =>
                Segments != null && Segments.Count >= 1
                    ? string.Join(" -> ", Segments.Select(s => $"{s.To}").ToList().Prepend($"{Segments.First().From}")) + $" [{TotalDistance}]"
                    : string.Empty;
    }
}
