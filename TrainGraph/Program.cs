﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainGraph.Common;
using TrainGraph.Train;

namespace TrainGraph
{
    class Program
    {
        static Tuple<char, char> ReadPointsPair()
        {
            return ConsoleUtils.TryRead("Enter two points separated by comma or spaces",
                          s =>
                          {
                              var chars = s.Replace(",", string.Empty).Replace(" ", string.Empty).ToUpperInvariant().ToCharArray();
                              return new Tuple<char, char>(chars[0], chars[1]);
                          });
        }


        static void Main(string[] args)
        {
            var filePath = args.FirstOrDefault();
            RailNetwork network = null;

            try
            {
                network = RailNetwork.LoadFromFile(filePath);

            }
            catch (Exception ex)
            {
                Console.WriteLine($"Cannot load netork from file: '{filePath}', error: {ex.Message}.");
                Console.WriteLine("Please please any key to exit.");
                Console.ReadLine();
                return;
            }
            while (true)
            {
                Console.WriteLine("");
                Console.WriteLine("Choose an options : ");
                Console.WriteLine(
                    "1) Distance of the route following way points");
                Console.WriteLine(
                    "2) Number of trips between from a starting point to a destination with max 3 stops");
                Console.WriteLine(
                    "3) Number of trips between from a starting point to a destination with exactly 4 stops");
                Console.WriteLine(
                    "4) The length of the shortest route (in terms of distance to travel) from a starting point to a destination");
                Console.WriteLine(
                    "5) The number of different routes from a starting point to a destination with a max of 30");



                var number = ConsoleUtils.TryReadNumber(1, 5);
                switch (number)
                {
                    case 1:
                    {
                        var wayPoints = ConsoleUtils.TryRead("Enter a list of points, separated by comma or spaces",
                            s =>
                                s.Replace(",", string.Empty).Replace(" ", string.Empty).ToUpperInvariant().ToCharArray());
                        var result = network.GetDistance(wayPoints);
                        Console.WriteLine(result == null ? "No such route" : $"Distance:{result}");
                    }
                        break;
                    case 2:
                    {
                        var points = ReadPointsPair();
                        var trips = network.GetAllTripsBetweenWithMaxDistance(points.Item1, points.Item2, 3);

                        Console.WriteLine($"trips found:{trips?.Count()}");
                        foreach (var trip in trips)
                        {
                            Console.WriteLine(trip.ToString());
                        }

                        }
                        break;
                    case 3:
                    {
                        var points = ReadPointsPair();
                        var trips = network.GetAllTripsBetweenWithExactDistance(points.Item1, points.Item2, 4);
                            Console.WriteLine($"trips found:{trips?.Count()}");
                            foreach (var trip in trips)
                            {
                                Console.WriteLine(trip.ToString());
                            }
                        }


                        break;
                    case 4:
                    {
                        var points = ReadPointsPair();
                        var trip = network.GetShortestPathBetween(points.Item1, points.Item2);
                        Console.WriteLine(trip);
                    }

                        break;
                    case 5:
                    {
                        var points = ReadPointsPair();
                        var trips = network.GetAllTripsBetweenWithMaxDistance(points.Item1, points.Item2, 30);
                            Console.WriteLine($"trips found:{trips?.Count()}");
                            foreach (var trip in trips)
                            {
                                Console.WriteLine(trip.ToString());
                            }

                        }
                        break;
                    default:
                        break;
                }

            }
        }


    }
}
