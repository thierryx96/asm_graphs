﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TrainGraph.Train;

namespace TrainGraph.Tests
{
    /*
 
1. The distance of the route A-B-C.
2. The distance of the route A-D.
3. The distance of the route A-D-C.
4. The distance of the route A-E-B-C-D.
5. The distance of the route A-E-D.
6. The number of trips starting at C and ending at C with a maximum of 3 stops.  In the sample data below, there are two such trips: C-D-C (2 stops). and C-E-B-C (3 stops).
7. The number of trips starting at A and ending at C with exactly 4 stops.  In the sample data below, there are three such trips: A to C (via B,C,D); A to C (via D,C,D); and A to C (via D,E,B).
8. The length of the shortest route (in terms of distance to travel) from A to C.
9. The length of the shortest route (in terms of distance to travel) from B to B.
10. The number of different routes from C to C with a distance of less than 30.  In the sample data, the trips are: CDC, CEBC, CEBCDC, CDCEBC, CDEBC, CEBCEBC, CEBCEBCEBC.

Test Input:
For the test input, the towns are named using the first few letters of the alphabet from A to D. A route between two towns (A to B) with a distance of 5 is represented as AB5.

Graph: AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7

Expected Output:
Output #1: 9
Output #2: 5
Output #3: 13
Output #4: 22
Output #5: NO SUCH ROUTE
Output #6: 2
Output #7: 3
Output #8: 9
Output #9: 9
Output #10: 7
*/
    [TestClass]
    public class TrainsAcceptanceTests
    {
        private RailNetwork _network = null;

        [TestInitialize]
        public void Init()
        {
            _network = new RailNetwork("AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7");
        }

        [TestMethod]
        public void DistanceOfAB_ShouldBe5()
        {
            AssertPathDistanceAreCorrect(5, new[] { 'A', 'B' });
        }

        private void AssertPathDistanceAreCorrect(int? expected, char[] route)
        {
            var actual = _network.GetDistance(route);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void DistanceOfABC_ShouldBe9()
        {
            AssertPathDistanceAreCorrect(9, new[] {'A', 'B', 'C'});
        }

        [TestMethod]
        public void DistanceOfAD_ShouldBe5()
        {
            AssertPathDistanceAreCorrect(5, new[] { 'A', 'D' });
        }

        [TestMethod]
        public void DistanceOfADC_ShouldBe13()
        {
            AssertPathDistanceAreCorrect(13, new[] { 'A', 'D' , 'C'});

        }

        [TestMethod]
        public void DistanceOfAEBCD_ShouldBe22()
        {
            AssertPathDistanceAreCorrect(22, new[] { 'A', 'E', 'B', 'C', 'D' });

        }

        [TestMethod]
        public void DistanceOfAED_ShouldNotExists()
        {

            AssertPathDistanceAreCorrect(null, new[] { 'A', 'E', 'D' });

        }

        // 6. The number of trips starting at C and ending at C with a maximum of 3 stops.  
        // In the sample data below, there are two such trips: 
        // C-D-C   (2 stops)
        // C-E-B-C (3 stops)
        [TestMethod]
        public void NumberOfTripsBetweenCAndC_WithMax3Stops_ShouldBe2()
        {

            var expectedTripWith2Stops = new char[] {'C', 'D', 'C'};            // C-D-C (2 stops)
            var expectedTripWith3Stops = new char[] { 'C', 'E', 'B', 'C' };     //C-E-B-C (3 stops).


            var trips = _network.GetAllTripsBetweenWithMaxStops('C', 'C', 3);

            Assert.AreEqual(2, trips.Count());

            // C-D-C (2 stops)
            var actualTripWith2Stops = trips.FirstOrDefault(v => v.Stops.Count - 1 == 2).Stops.ToArray();
            Assert.IsNotNull(actualTripWith2Stops);
            CollectionAssert.AreEqual(expectedTripWith2Stops, actualTripWith2Stops);

            //C-E-B-C (3 stops)
            var actualTripWith3Stops = trips.FirstOrDefault(v => v.Stops.Count - 1 == 3).Stops.ToArray();
            Assert.IsNotNull(actualTripWith3Stops);
            CollectionAssert.AreEqual(expectedTripWith3Stops, actualTripWith3Stops);

        }

        // 7. The number of trips starting at A and ending at C with exactly 4 stops.  
        // In the sample data below, there are three such trips: 
        // A to C (via B,C,D) 4 stops
        // A to C (via D,C,D) 4 stops
        // A to C (via D,E,B) 4 stops
        [TestMethod]
        public void NumberOfTripsBetweenAAndC_WithExactly4Stops_ShouldBe3()
        {
            var expectedTrips = new[]
            {
               "ABCDC",
               "ADCDC",
               "ADEBC"           
            }
            .Select(t => t.ToCharArray());
            //var expectedTrip1 = new char[] { 'A', 'B', 'C', 'D', 'C' };// A to C (via B,C,D)
            //var expectedTrip2 = new char[] { 'A', 'D', 'C', 'D', 'C' };// A to C (via D,C,D)
            //var expectedTrip3 = new char[] { 'A', 'D', 'E', 'B', 'C' };// A to C (via D,E,B)

            var actualTrips = _network.GetAllTripsBetweenWithExactStops('A', 'C', 4);

            Assert.AreEqual(expectedTrips.Count(), actualTrips.Count());

            foreach (var expectedTrip in expectedTrips)
            {
                Assert.IsTrue(actualTrips.Any(t => t.Stops.SequenceEqual(expectedTrip)));
            }

            // C-D-C (2 stops)
            //var actualTripWith2Stops = trips.FirstOrDefault(v => v.Stops.Count - 1 == 2).Stops.ToArray();
            //Assert.IsNotNull(actualTripWith2Stops);
            //CollectionAssert.AreEqual(expectedTripWith2Stops, actualTripWith2Stops);

            ////C-E-B-C (3 stops)
            //var actualTripWith3Stops = trips.FirstOrDefault(v => v.Stops.Count - 1 == 3).Stops.ToArray();
            //Assert.IsNotNull(actualTripWith3Stops);
            //CollectionAssert.AreEqual(expectedTripWith3Stops, actualTripWith3Stops);

        }

      


        // 8. The length of the shortest route(in terms of distance to travel) from A to C. => 9
        [TestMethod]
        public void ShortestPath_BetweenAAndC_ShouldBe9()
        {
            var expectedDistance = 9;

            var actualShortestTrip = _network.GetShortestPathBetween('A', 'C');

            Assert.AreEqual(expectedDistance, actualShortestTrip.TotalDistance);

        }

        // 9. The length of the shortest route (in terms of distance to travel) from B to B. => 9
        [TestMethod]
        public void ShortestPath_BetweenBAndB_ShouldBe9()
        {
            var expectedDistance = 9;

            var actualShortestTrip = _network.GetShortestPathBetween('B', 'B');

            Assert.AreEqual(expectedDistance, actualShortestTrip.TotalDistance);
        }

        // 10. The number of different routes from C to C with a distance of less than 30.  
        // In the sample data, the trips are: 
        // CDC 
        // CEBC 
        // CEBCDC
        // CDCEBC
        // CDEBC
        // CEBCEBC
        // CEBCEBCEBC.
        [TestMethod]
        public void NumberOfTripsBetweenAAndC_WithMaxDistance_ShouldBe7()
        {

            var expectedTrips = new[]
            {
                "CDC",
                "CEBC",
                "CEBCDC",
                "CDCEBC",
                "CDEBC",
                "CEBCEBC",
                "CEBCEBCEBC"
            }.Select(t => t.ToCharArray());

            var actualTrips = _network.GetAllTripsBetweenWithMaxDistance('C', 'C', 30);

            Assert.AreEqual(expectedTrips.Count(), actualTrips.Count());

            foreach (var expectedTrip in expectedTrips)
            {
                Assert.IsTrue(actualTrips.Any(t => t.Stops.SequenceEqual(expectedTrip)));
            }
        }
    }
}
