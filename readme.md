﻿# Overview (where do I start?)

## Content

* Proposed solution source code with C# with Visual Studio 2015.
* the executable can be found at the root of this repo, within /TrainGraph.zip 

## Prerequisites

* MS .net Framework 4.6.1
* Git to get the code (optional)
* Visual Studio 2013 or 2015 to open and edit it (optional)

## Run the executable

The executable version of this program can found at the root of this repo /ColourFour.Console.zip 


```
TrainGraph trains.txt
```

The executable must with a file path provided as command line parameter. The content of the file is a list
of nodes pairs including the distance (i.e. edges), seperated by whitespaces, or comma. One edge per line is also valid.

```
AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7
```

```
AB5
BC4
CD8
DC8
DE6
AD5
CE2
EB3
AE7
```

## Run in Debug Mode (Visual Studio 2013)

* Open the .sln file
* Run the main project (ColourFour.Console) as debug (the file included, trains.txt will be automatically used to build the graph)

## Output 
```
TrainGraph.exe trains.txt
```

```
Choose an options :
1) Distance of the route following way points
2) Number of trips between from a starting point to a destination with max 3 stops
3) Number of trips between from a starting point to a destination with exactly 4 stops
4) The length of the shortest route (in terms of distance to travel) from a starting point to a destination
5) The number of different routes from a starting point to a destination with a max of 30
Please enter a number between '1' and '5':5
Enter two points separated by comma or spaces
A B
trips found:10
A -> B -> C -> D -> E -> B [26]
A -> B -> C -> E -> B [14]
A -> B -> C -> E -> B -> C -> E -> B [23]
A -> D -> C -> E -> B [18]
A -> D -> C -> E -> B -> C -> E -> B [27]
A -> D -> E -> B [14]
A -> D -> E -> B -> C -> E -> B [23]
A -> E -> B [10]
A -> E -> B -> C -> E -> B [19]
A -> E -> B -> C -> E -> B -> C -> E -> B [28]

Choose an options :
1) Distance of the route following way points
2) Number of trips between from a starting point to a destination with max 3 stops
3) Number of trips between from a starting point to a destination with exactly 4 stops
4) The length of the shortest route (in terms of distance to travel) from a starting point to a destination
5) The number of different routes from a starting point to a destination with a max of 30
Please enter a number between '1' and '5':

...
```

## Project Structure
```
/Common
   CollectionExtensions.cs : helpers for dictionaries and generic collections.
   ConsoleUtils.cs : helpers for a console application (input parsers, etc ... )

/Graphs
  Graph.cs : provide way to create, manipulate and perform basic operations on graphs
  IGraph.cs : define the graph operations
  Connection & Edge : define some component used in a graph.

/Trains
  RailNetwork.cs : wrapper around a graph, providing helpers and operations for the context of this assessement
  Trip.cs : helper to define a train route, and print it

Program.cs : entry point (main)
trains.txt : example input file containing the train network given in the instructions
```


# Design (why it is done this way?)

## Domain

The overall domain design is pretty straightforward to me, a graph class define a graph data structure and a RailNetwork class wraps it with more user-friendly, domain-oriented operations.

## Choices / decision / assumptions

### Graph implementation

The graph has been implemented by using an Adjacency List (over an adjacency matrix). Mainly based on the assumption that a train network is not a dense network ;  an adjancy matrix would be performance-wise a better choice in this case.

For the Adjacency List implementation every node got it list of connection (node connected as destination)

- s: source node
- d: destination node
- w: distance

```
 (s1) -> { (d3, w3), (d2, w2) ... }
 (s2) -> { }
 (s3) -> { (d1, w1) ... }
```

defines :

```
 +----+
 |    |
 |    v
 1 -> 3
 |
 v
 2

(distance not included)
```

### Algorithm to get the shortest path

In order to search routes between nodes and more particularly the shortest routes plenty of algorythms are available to solve this well-known problem. 

I have implemented the path finding using the DFS (Depth first search - ) algorithm, due to the fact that it is natural and easy to undertsand. And with adding some constraints I could solve all the problems from the instructions :

Basically searching for all routes between two points, based on some constraints :
- Max nodes in the path (solves :  The number of trips starting at C and ending at C with a maximum of 3 stops).
- Max distance of the path (solves : The number of different routes from C to C with a distance of less than 30).
- Only travel the path once, avoid cycles (solves : The length of the shortest route (in terms of distance to travel) from A to C)


Pseudo code
```
SearchPath(source, distance = 0, target, path = [], nodesLimit = null, distanceLimit = null, excludeCycle = false)
           
if (source == target )  // find a valid path
  add path to global list
      
for(n in source.neighbours)
   candidatePath = path + n // including neighbours in the path
   candidateDistance = distance(source to n)
   
   if(candidatePath & candidateDistance satisfy all defined constraints (nodesLimit, distanceLimit, excludeCycle))
     SearchPath(n, candidateDistance, target, candidatePath, ... )

```

This algorithm can be really time & ressource consuming for huge graphs, in this case I would advise to use Djikstra (https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm) algorith instead (given that we are excluding any cycle all the time) 

# Testing (does it work?)

## Given Scenarios (acceptance tests) ##

The scenarios provided in the instructions has been used to define the test cases of this solution. 


1. The distance of the route A-B-C. 
2. The distance of the route A-D.
3. The distance of the route A-D-C.
4. The distance of the route A-E-B-C-D.
5. The distance of the route A-E-D.
6. The number of trips starting at C and ending at C with a maximum of 3 stops.  In the sample data below, there are two such trips: C-D-C (2 stops). and C-E-B-C (3 stops).
7. The number of trips starting at A and ending at C with exactly 4 stops.  In the sample data below, there are three such trips: A to C (via B,C,D); A to C (via D,C,D); and A to C (via D,E,B).
8. The length of the shortest route (in terms of distance to travel) from A to C.
9. The length of the shortest route (in terms of distance to travel) from B to B.
10. The number of different routes from C to C with a distance of less than 30.  In the sample data, the trips are: CDC, CEBC, CEBCDC, CDCEBC, CDEBC, CEBCEBC, CEBCEBCEBC.


For every scenario a unit test has been build 

```
1. The distance of the route A-B-C. => DistanceOfAB_ShouldBe5

...
```


It does does cover most of the graph operation, but do not a provide a sufficient coverage to test (with more time this could be achieved)
- edge cases 
- differents graph types (two seperates networks, distances all equals (no-weighted graph) ... )

Also the console and user inputs hasn't been fully tested.